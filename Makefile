make: build/server build/main
	sudo ./build/main

build/main: src/main.c src/utils.c
	@mkdir -p build
	gcc -g -Wall -o $@ $^

build/server: src/utils.c src/hashtable/hash.c src/hashtable/list.c src/hashtable/stats.c src/hashtable/hash_table.c src/hashtable/wrapper.c src/server/bin_socket.c  src/server/text_socket.c src/server/main.c src/server/text_commands.c 
	@mkdir -p build
	gcc -g -Wall -pthread $^ -o $@ 


debug: build/server build/main
	sudo env DO_DEBUG_LOG=TRUE ./build/main
