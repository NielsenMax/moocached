-module(test).

-import(client,
        [start/0,
         start/1,
         put/3,
         put/4,
         del/2,
         del/3,
         get/2,
         get/3,
         take/2,
         take/3,
         stats/1,
         stats/2,
         close/1,
         parse_stats/1]).

-export([startTest/1]).

test(N) ->
    case start() of
        {ok, Conn} ->
            put(Conn,a,N),
            G = get(Conn,a),
            io:fwrite("~p ~p ~n", [N, G]),
            close(Conn)
            % enotfound = get(Conn, hola, 10000),
            ;
        Error ->
            Error
    end.

startTest(N) ->
    case N of
        0 ->
            ok;
        _ ->
            spawn(fun() -> test(N) end),
            startTest(N - 1)
    end.

