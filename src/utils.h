#ifndef __UTILS_H__
#define __UTILS_H__
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <stdbool.h>

#define FDEAGAIN -1
#define FDEND -2
#define FDERROR -3


static bool __attribute__ ((unused)) DO_DEBUG = false;

void init_debug_log();
int debug_log(const char *fmt, ...);

int read_to_buffer(int fd, char *buffer, size_t *cursor, size_t max);

void quit(char *s);
bool alfanumeric(char *s, int len);

#endif