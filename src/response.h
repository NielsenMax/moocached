#ifndef __RESPONSE_H__
#define __RESPONSE_H__

enum responds
{
    OK,
    ENOTFOUND,
    OOMERROR,
    MAXRETRIES,
    EINVALID,
    EBIG,
    ERROR,
    EBINARY,
    CONNEND
};

#endif