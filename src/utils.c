#include "utils.h"

#include <stdio.h>
#include <ctype.h>

void init_debug_log() {
    DO_DEBUG = (getenv("DO_DEBUG_LOG") != NULL);
}

int debug_log(const char* fmt, ...)
{
    if (DO_DEBUG) {
        va_list ap;
        va_start(ap, fmt);
        vfprintf(stderr, fmt, ap);
        va_end(ap);
        return 0;
    }
    return 1;
}


int read_to_buffer(int fd, char* buffer, size_t* cursor, size_t max)
{
    while (*cursor < max)
    {
        int readed = read(fd, buffer + *cursor, max - *cursor);
        if (readed < 0)
        {
            if (errno == EAGAIN || errno == EWOULDBLOCK)
            {
                return FDEAGAIN;
            }
            else
            {
                return FDERROR;
            }
        }
        if (readed == 0)
        {
            return FDEND;
        }
        *cursor += readed;
    }
    return 0;
}

void quit(char* s)
{
    perror(s);
    abort();
}

bool alfanumeric(char* s, int len)
{

    for (int i = 0; i < len; i++)
    {
        if (!isalpha(s[i]) && s[i] != ' ' && s[i] < 48 && s[i] > 57)
            return false;
    }
    return true;
}