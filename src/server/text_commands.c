#include "text_commands.h"

void command_init(Command* cmd) {
    memset(cmd, 0, sizeof(*cmd));
}

int run_command(Wrapper* wr, Command* cmd, char **out, size_t * len) {
    switch (cmd->action)
    {
    case STATS:
        return wrapper_stats(wr, out, len);
    case PUT: ;
        char* key = wrapper_tryalloc(wr, cmd->lenkey);
        if (key == NULL) return OOMERROR;
        memcpy(key, cmd->key, cmd->lenkey);
        char* value = wrapper_tryalloc(wr, cmd->lenvalue);
        if (value == NULL) {
            free(key);
            return OOMERROR;
        }
        memcpy(value, cmd->value, cmd->lenvalue);
        return wrapper_put(wr, key, value, cmd->lenkey, cmd->lenvalue);
    case DEL:
        return wrapper_del(wr, cmd->key, cmd->lenkey);
    case GET:
        return wrapper_get(wr, cmd->key, cmd->lenkey, out, len);
    case TAKE:
        return wrapper_take(wr, cmd->key, cmd->lenkey, out, len);
    default:
        return EINVALID;
    }
}