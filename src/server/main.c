#define _GNU_SOURCE
#include <sys/epoll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/resource.h>
#include <sys/types.h>
#include "text_socket.h"
#include "bin_socket.h"
#include "../hashtable/hash_table.h"
#include "../utils.h"
#include <signal.h>
#include <fcntl.h>

#define MAX_EVENTS 10
typedef struct Server_data
{
	Wrapper *wr;
	int text_socket, bin_socket;
	int epfd;
} Server_data;

void *server(void *data);

typedef struct Ev_data
{
	int fd;
	bool bin;
	void *data;
} Ev_data;

int setnonblocking(int fd)
{

	int flags = fcntl(fd, F_GETFL, 0);
	if (flags == -1)
	{
		debug_log("fcntl F_GETFL");
		return -1;
	}

	if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1)
	{
		debug_log("fcntl F_SETFL O_NONBLOCK");
		return -1;
	}
	return 0;
}

void free_ev_data(Ev_data *ev_data)
{
	if (ev_data == NULL)
		return;

	if (ev_data->bin)
	{
		free_bin_data(ev_data->data);
	}
	else
	{
		free_text_data(ev_data->data);
	}
	free(ev_data);
}

typedef int(CreateData)(Wrapper *wr, void **data);

int create_special_data(Wrapper *wr, void **data)
{
	return 0;
}

Ev_data *create_ev_data(Wrapper *wr, int fd, bool bin, CreateData f)
{

	Ev_data *ev_data = wrapper_tryalloc(wr, sizeof(Ev_data));

	if (ev_data == NULL)
		return NULL;

	ev_data->fd = fd;
	ev_data->bin = bin;

	ev_data->data = NULL;
	if (f(wr, &ev_data->data) != 0)
	{ /// liberamos memoria, algo salio mal.
		free(ev_data);
		return NULL;
	}

	return ev_data;
}

void add_epoll_socket(Wrapper *wr, int epfd, int socket, bool bin, CreateData createdata, bool client)
{

	struct epoll_event ev;

	ev.events = EPOLLIN | EPOLLONESHOT;

	if (client)
		ev.events |= EPOLLRDHUP;

	ev.data.ptr = create_ev_data(wr, socket, bin, createdata);

	if (ev.data.ptr == NULL)
	{
		debug_log("Cerramos la conexion %d\n", socket);
		close(socket);
		return;
	}

	if (epoll_ctl(epfd, EPOLL_CTL_ADD, socket, &ev) == -1)
	{
		debug_log("FALLO ADD:Cerramos la conexion %d\n", socket);
		close(socket);
		free_ev_data(ev.data.ptr);
	}
}

void readd_epoll_socket(int epfd, int socket, void *ptrdata, bool client)
{

	struct epoll_event ev;
	ev.events = EPOLLIN | EPOLLONESHOT;

	if (client)
		ev.events |= EPOLLRDHUP;

	ev.data.ptr = ptrdata;
	if (epoll_ctl(epfd, EPOLL_CTL_MOD, socket, &ev) == -1)
	{
		debug_log("FALLO READD: para el fd %d %d\n", socket, errno);
		close(socket);

		free_ev_data(ptrdata);
	}
}

void accept_new_connection(Wrapper *wr, int epfd, int fd, bool bin){
		int conn_sock = accept(fd, NULL, NULL);

		if (conn_sock == -1)
		{
			debug_log("accept error");
			return;
		}

		if (setnonblocking(conn_sock) != 0)
			return;

		if (bin)
		{
			add_epoll_socket(wr, epfd, conn_sock, bin, create_bin_data, true);
		}
		else
		{
			add_epoll_socket(wr, epfd, conn_sock, bin, create_text_data, true);
		}
}

void *server(void *data) // HT ht, int text_socket, int bin_socket
{
	Server_data *rdata = data;
	int epfd = rdata->epfd;
	int text_socket = rdata->text_socket;
	int bin_socket = rdata->bin_socket;
	Wrapper *wr = rdata->wr;

	while (1)
	{
		struct epoll_event events[MAX_EVENTS];
		int nevents = epoll_wait(epfd, events, MAX_EVENTS, -1); /// devuelve la cantidad de eventos a manejar

		if (nevents <= 0 && errno == EINTR)
			continue;

		for (int i = 0; i < nevents; i++)
		{

			Ev_data *ev_data = (events[i].data.ptr);
			int fd = ev_data->fd;
			bool bin = ev_data->bin;

			if (events[i].events & (EPOLLHUP | EPOLLRDHUP | EPOLLERR))
			{
				debug_log("FDERROR\n");
				continue;
			}

			if (fd == text_socket || fd == bin_socket)
			{

				bool binsocket = fd == bin_socket;
				accept_new_connection(wr,epfd,fd, binsocket);

				readd_epoll_socket(epfd, fd, ev_data, false);

				continue;
			}

			/// nos habla una conexion ya abierta.
			bool connection;
			if (bin)
			{
				connection = answer_bin(wr, fd, ev_data->data);
			}
			else
			{
				connection = answer_text(wr, fd, ev_data->data);
			}
			if (connection)
			{
				readd_epoll_socket(epfd, fd, ev_data, true);
			}
			else
			{
				debug_log("Cerramos la conexion %d\n", fd);
				free(ev_data);
				close(fd);
			}
		}
	}
}



int main(int argc, char **argv)
{
	init_debug_log();

	struct sigaction SIGAC;
	SIGAC.sa_handler = SIG_IGN;

	sigaction(SIGPIPE, &SIGAC, NULL);

	uid_t uid = getuid();

	if (uid == 0)
	{
		quit("EpollServer tiene permisos root");
	}

	Wrapper *wr = wrapper_create();
	if (wr == NULL)
	{
		quit("hashtable_create - Error");
	}

	int text_socket = atoi(argv[1]);
	int bin_socket = atoi(argv[2]);

	int epfd = epoll_create1(0);
	if (epfd == -1)
	{
		quit("epoll_create");
	}

	add_epoll_socket(wr, epfd, bin_socket, true, create_special_data, false);
	add_epoll_socket(wr, epfd, text_socket, false, create_special_data, false);

	long nprocessors = sysconf(_SC_NPROCESSORS_ONLN);

	pthread_t t[nprocessors];

	Server_data rdata;
	rdata.bin_socket = bin_socket;
	rdata.text_socket = text_socket;
	rdata.epfd = epfd;
	rdata.wr = wr;

	for (int i = 0; i < nprocessors; i++)
	{
		pthread_create(&t[i], NULL, &server, &rdata);
	}


	for (int i = 0; i < nprocessors; i++)
	{
		pthread_join(t[i], NULL);
	}

	return 0;
}
