#ifndef __BSOCKET_H__
#define __BSOCKET_H__

#include "../hashtable/wrapper.h"
#include "../utils.h"
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define OP_PUT 11
#define OP_DEL 12
#define OP_GET 13
#define OP_TAKE 14
#define OP_STATS 21

typedef struct bs_data
{
    int status; /// cambiar a enum

    char buff[4];

    size_t index;

     char operation;

    size_t lenkey;
     char *key;

    size_t lenvalue;
     char *value;

} bs_data;

void free_bin_KV(bs_data *bdata);

int create_bin_data(Wrapper *wr, void **data);

void free_bin_data(bs_data *bdata);

bool answer_bin(Wrapper *wr, int fd, bs_data *bdata);

#endif