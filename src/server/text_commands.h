#ifndef __COMMANDS_H__
#define __COMMANDS_H__

#include <stdlib.h>
#include "../hashtable/wrapper.h"

#define BUFFER_SIZE 2048

typedef struct command Command;

enum actions
{
    INVALID,
    PUT,
    GET,
    TAKE,
    DEL,
    STATS
};

struct command {
    enum actions action;

    size_t lenkey;
    char key[BUFFER_SIZE];

    size_t lenvalue;
    char value[BUFFER_SIZE];
};

void command_init(Command* cmd);
int run_command(Wrapper* wr, Command* cmd, char **out, size_t * len);

#endif
