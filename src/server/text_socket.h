#ifndef __TSOCKET_H__
#define __TSOCKET_H__
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include "../utils.h"
#include "../hashtable/wrapper.h"
#include "text_commands.h"

typedef struct ts_data
{
	char buffer[BUFFER_SIZE];
    size_t size;
} ts_data;

void free_text_data(ts_data *tdata);
int create_text_data(Wrapper *wr, void **data);
void free_text_KV(ts_data *tdata);

char *substring_copy(Wrapper *wr,  char *s, int st, int end);
bool answer_text(Wrapper *wr, int fd, ts_data *tdata);

#endif