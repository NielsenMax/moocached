
#include "text_socket.h"

void free_text_data(ts_data* tdata)
{
	if (tdata != NULL)
		free(tdata);
}

int create_text_data(Wrapper* wr, void** data)
{
	ts_data* datap = wrapper_tryalloc(wr, sizeof(*datap));
	*data = datap;
	if (*data == NULL)
		return -1;
	memset(datap, 0, sizeof(*datap));
	return 0;
}

bool print_result(int fd, int result)
{
	char* reply;
	switch (result)
	{
	case OK:
		reply = "OK\n";
		break;
	case ENOTFOUND:
		reply = "ENOTFOUND\n";
		break;
	case OOMERROR:
		reply = "OOMERROR\n";
		break;
	case MAXRETRIES:
		reply = "MAXRETRIES\n";
		break;
	case EINVALID:
		reply = "EINVAL\n";
		break;
	case EBIG:
		reply = "EBIG\n";
		break;
	case ERROR:
		reply = "ERROR\n";
	case EBINARY:
		reply = "EBINARY\n";
		break;
	}

	int wresult = write(fd, reply, strlen(reply));
	return wresult != -1;
}

bool reply_ok(int fd, char* value, int lenvalue)
{

	if (!alfanumeric(value, lenvalue))
	{
		return print_result(fd, EBINARY);
	}

	if (lenvalue + 4 > BUFFER_SIZE)
	{
		return print_result(fd, EBIG);
	}

	char reply[lenvalue + 4];
	int len = lenvalue + 4;
	// Tenemos que usar snprintf porque value no tiene terminador
	snprintf(reply, len, "OK %s", value);
	reply[len++] = '\n'; // Ponemos el \n despues porque sino nos cortaria snprintf
	int result = write(fd, reply, len);
	return result != -1;
}

bool line_begins(char* str, char* end, char* cmp) {
	size_t len = strlen(cmp);
	if (end - str < len) return false;
	if (memcmp(str, cmp, len) != 0) return false;
	return (end - str == len) || (str[len] == ' ');
}

char* skip_spaces(char* str, char* end) {
	char* ret = memchr(str, ' ', end - str);
	if (ret == NULL) return str;
	while (*ret == ' ') {
		ret = ret + 1;
		if (end - ret == 0) return ret;
	}
	return ret;
}

enum ParseStatus {
	PARSE_INVALID,
	PARSE_VALID,
	PARSE_INCOMPLETE
};

int parse_text(char** line, char* end, Command* cmd) {
	char* start = *line;
	char* line_end = memchr(start, '\n', end - start);
	if (line_end == NULL) return PARSE_INCOMPLETE;
	*line = line_end + 1;

	while(*start == ' ') /// ignoramos los espacios del principio
		start++;

	if (!alfanumeric(start, line_end - start)) return PARSE_INVALID;

	if (line_begins(start, line_end, "STATS")) cmd->action = STATS;
	else if (line_begins(start, line_end, "GET")) cmd->action = GET;
	else if (line_begins(start, line_end, "PUT")) cmd->action = PUT;
	else if (line_begins(start, line_end, "TAKE")) cmd->action = TAKE;
	else if (line_begins(start, line_end, "DEL")) cmd->action = DEL;
	else return PARSE_INVALID;
	
	switch (cmd->action)
	{
	case STATS:
	{
		char* after_space = skip_spaces(start, line_end);
		if (after_space != NULL && after_space != start && line_end != after_space) return PARSE_INVALID;
		return PARSE_VALID;
	}
	case GET: case DEL: case TAKE:
	{
		char* action_end = skip_spaces(start, line_end);
		if (action_end == NULL) return PARSE_INVALID;
		char* key_start = action_end;
		if (key_start == line_end) return PARSE_INVALID;
		char* key_end = memchr(key_start, ' ', line_end - key_start);
		if (key_end == NULL) key_end = line_end;
		char* after_space = skip_spaces(key_end, line_end);
		if (after_space != NULL && line_end != after_space) return PARSE_INVALID;
		cmd->lenkey = key_end - key_start;
		memcpy(cmd->key, key_start, cmd->lenkey);
		return PARSE_VALID;
	}
	case PUT:
	{
		char* action_end = skip_spaces(start, line_end);
		if (action_end == NULL) return PARSE_INVALID;
		char* key_start = action_end;
		if (key_start == line_end) return PARSE_INVALID;
		char* key_end = memchr(key_start, ' ', line_end - key_start);
		if (key_end == NULL) return PARSE_INVALID;
		char* after_space = skip_spaces(key_end, line_end);
		if (after_space != NULL && line_end == after_space) return PARSE_INVALID;
		char* value_start = after_space;
		if (value_start == line_end) return PARSE_INVALID;
		char* value_end = memchr(value_start, ' ', line_end - value_start);
		if (value_end == NULL) value_end = line_end;
		after_space = skip_spaces(value_end, line_end);
		if (after_space != NULL && line_end != after_space) return PARSE_INVALID;
		cmd->lenkey = key_end - key_start;
		memcpy(cmd->key, key_start, cmd->lenkey);
		cmd->lenvalue = value_end - value_start;
		memcpy(cmd->value, value_start, cmd->lenvalue);
		return PARSE_VALID;
	}
	default:
		return PARSE_INVALID;
	}
}

bool answer_text(Wrapper* wr, int fd, ts_data* tdata) {
	int status = read_to_buffer(fd, tdata->buffer, &tdata->size, BUFFER_SIZE);
	if (status == FDERROR)
		return false;
	if (status == FDEND && tdata->size == 0)
		return false;

	char* buffer = tdata->buffer;
	char* end = buffer + tdata->size;
	while (buffer != end) {
		Command cmd;
		command_init(&cmd);
		int status = parse_text(&buffer, end, &cmd);
		if (status == PARSE_INCOMPLETE) {
			if (end - buffer == BUFFER_SIZE) {
				print_result(fd, EBIG);
				return false;
			};
			break;
		}
		else if (status == PARSE_INVALID) {
			if (!print_result(fd, EINVALID))
				return false;
			continue;
		}
		char* out = NULL;
		size_t len = 0;
		int result = run_command(wr, &cmd, &out, &len);
		switch (cmd.action)
		{
		case STATS:
		{
			char* reply = "OK ";
			int wresult = write(fd, reply, strlen(reply));
			if (wresult < 0) {
				free(out);
				return false;
			}
			int status = write(fd, out, len);
			free(out);
			if (status < 0) {
				return false;
			}
			reply = "\n";
			wresult = write(fd, reply, strlen(reply));
			if (wresult < 0) {
				free(out);
				return false;
			}
			continue;
		}
		case PUT: case DEL:
		{
			if (!print_result(fd, result))
				return false;
			continue;
		}
		case GET: case TAKE:
		{
			if (result != OK)
			{
				free(out);
				if (!print_result(fd, result))
					return false;
				continue;
			}
			result = reply_ok(fd, out, len);
			free(out);
			if (result == false)
				return false;
			continue;
		}
		default:
			continue;
		}
	}
	tdata->size = end - buffer;
	memmove(tdata->buffer, buffer, tdata->size);
	return true;

}