#include "bin_socket.h"

void free_bin_data(bs_data* bdata)
{
	if (bdata == NULL)
		return;

	if (bdata->key != NULL)
		free(bdata->key);
	if (bdata->value != NULL)
		free(bdata->value);
	free(bdata);
}

void free_bin_KV(bs_data* bdata)
{
	if (bdata == NULL)
		return;

	if (bdata->key != NULL)
	{
		free(bdata->key);
		bdata->key = NULL;
	}
	if (bdata->value != NULL)
	{
		free(bdata->value);
		bdata->value = NULL;
	}

	bdata->key = NULL;
	bdata->value = NULL;
}

int create_bin_data(Wrapper* wr, void** data)
{
	bs_data* datap = wrapper_tryalloc(wr, sizeof(*datap));
	*data = datap;
	if (*data == NULL)
		return -1;

	memset(datap, 0, sizeof(*datap));
	return 0;
}

bool print_result_bin(int fd, int result)
{

	char c;
	switch (result)
	{
	case OK:
		c = 101;
		break;
	case ENOTFOUND:
		c = 112;
		break;
	case OOMERROR:
		c = 115;
		break;
	case MAXRETRIES:
		c = 115;
		break;
	case EINVALID:
		c = 111;
		break;
	case EBIG:
		c = 114;
		break;
	case ERROR:
		c = 115;
		break;
	}

	int wresult = write(fd, &c, 1);
	return wresult != -1;
}

bool put_bin(Wrapper* wr, int fd, char* key, size_t lenkey, char* value, size_t lenvalue)
{
	int result = wrapper_put(wr, key, value, lenkey, lenvalue);
	return print_result_bin(fd, result);
}

bool del_bin(Wrapper* wr, int fd, char* key, size_t lenkey)
{
	int result = wrapper_del(wr, key, lenkey);
	return print_result_bin(fd, result);
}

int print_ok_value(int fd, size_t lenvalue, char* value)
{
	int len_s = htonl(lenvalue);
	int result;

	char c = 101;
	result = write(fd, &c, 1); /// OK
	if (result == -1)
		return false;
	result = write(fd, &len_s, 4); /// LARGO
	if (result == -1)
		return false;
	result = write(fd, value, lenvalue); /// CLAVE
	return result != -1;
}

bool get_bin(Wrapper* wr, int fd, char* key, size_t lenkey)
{

	char* value;
	size_t lenvalue;
	int result = wrapper_get(wr, key, lenkey, &value, &lenvalue);

	if (result != OK)
	{
		return print_result_bin(fd, result);
	}

	return print_ok_value(fd, lenvalue, value);
}

bool take_bin(Wrapper* wr, int fd, char* key, size_t lenkey)
{

	char* value;
	size_t lenvalue;
	int result = wrapper_take(wr, key, lenkey, &value, &lenvalue);

	if (result != OK)
	{
		return print_result_bin(fd, result);
	}

	result = print_ok_value(fd, lenvalue, value);
	free(value);
	return result != -1;
}

bool stats_bin(Wrapper* wr, int fd)
{
	char* reply = NULL;
	size_t len;
	wrapper_stats(wr, &reply, &len);

	char c = 101;
	int result = write(fd, &c, 1);
	if (result == -1)
	{
		return false;
	}

	int len_s = htonl(len);
	if (write(fd, &len_s, 4) == -1)
	{
		return false;
	}

	result = write(fd, reply, len);
	if (result == -1)
	{
		return false;
	}
	return result != -1;
}

bool valid_operation(char OP)
{
	return OP == OP_PUT || OP == OP_DEL || OP == OP_GET || OP == OP_TAKE || OP == OP_STATS;
}

bool answer_bin(Wrapper* wr, int fd, bs_data* bdata)
{
	int error;
	while (true) {
		switch (bdata->status)
		{
		case 0:

			if ((error = read_to_buffer(fd, &bdata->operation, &bdata->index, 1)) != 0)
				goto error_nothing;

			bdata->index = 0;

			if (!valid_operation(bdata->operation))
			{
				memset(bdata, 0, sizeof(*bdata));
				bool res = print_result_bin(fd, EINVALID);
				if (!res) {
					return false;
				}
				continue;
			}

			if (bdata->operation == OP_STATS)
			{
				memset(bdata, 0, sizeof(*bdata));
				bool res = stats_bin(wr, fd);
				if (!res) {
					return false;
				}
				continue;
			}

			bdata->status++;
			bdata->index = 0;
		case 1:
			if ((error = read_to_buffer(fd, bdata->buff, &bdata->index, 4)) == 0)
			{
				bdata->lenkey = ntohl(*(uint32_t*)bdata->buff);
			}
			else
			{
				goto error_nothing;
			}
			bdata->index = 0;

			bdata->key = wrapper_tryalloc(wr, bdata->lenkey);
			if (bdata->key == NULL)
			{
				error = OOMERROR;
				goto error_nothing;
			}

			bdata->status++; /// vamos a empezar a leer la key
			bdata->index = 0;

		case 2:
			if ((error = read_to_buffer(fd, bdata->key, &bdata->index, bdata->lenkey)) != 0)
				goto free_key;
			bdata->index = 0;

			bdata->status++; /// ya tenemos la key leida

			if (bdata->operation == OP_GET)
			{
				bool res = get_bin(wr, fd, bdata->key, bdata->lenkey);

				free(bdata->key);
				memset(bdata, 0, sizeof(*bdata));
				if (!res) {
					return false;
				}
				continue;
			}
			if (bdata->operation == OP_TAKE)
			{
				bool res = take_bin(wr, fd, bdata->key, bdata->lenkey);
				free(bdata->key);
				memset(bdata, 0, sizeof(*bdata));
				if (!res) {
					return false;
				}
				continue;
			}
			if (bdata->operation == OP_DEL)
			{
				bool res = del_bin(wr, fd, bdata->key, bdata->lenkey);
				free(bdata->key);
				memset(bdata, 0, sizeof(*bdata));
				if (!res) {
					return false;
				}
				continue;
			}
			bdata->index = 0;

		case 3:

			if ((error = read_to_buffer(fd, bdata->buff, &bdata->index, 4)) == 0)
			{
				bdata->lenvalue = ntohl(*(uint32_t*)bdata->buff);
			}
			else
			{
				goto free_key;
			}
			bdata->index = 0;

			bdata->value = wrapper_tryalloc(wr, bdata->lenvalue);
			if (bdata->value == NULL)
			{
				error = OOMERROR;
				goto error_nothing;
			}

			bdata->status++; /// vamos a empezar a leer el value
			bdata->index = 0;

		case 4:
			if ((error = read_to_buffer(fd, bdata->value, &bdata->index, bdata->lenvalue)) != 0)
				goto free_key_value;

			bool res = put_bin(wr, fd, bdata->key, bdata->lenkey, bdata->value, bdata->lenvalue);
			memset(bdata, 0, sizeof(*bdata));
			if (!res) {
				return false;
			}
			continue;
		}
	}
free_key_value:
	if (error == FDEAGAIN)
	{
		debug_log("EAGAIN\n");
		return true;
	}
	free(bdata->value);
	bdata->value = NULL;
free_key:
	if (error == FDEAGAIN)
	{
		debug_log("EAGAIN\n");
		return true;
	}
	free(bdata->key);
	bdata->key = NULL;
error_nothing:
	if (error == FDEAGAIN)
	{
		debug_log("EAGAIN en el fd %d\n", fd);
		return true;
	}
	debug_log("Cerramos la conexion por error %d\n", error);
	return false;
}