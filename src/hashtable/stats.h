#ifndef __STATS_H__
#define __STATS_H__

#include <stdatomic.h>
#include <stdlib.h>

#include "../utils.h"

struct _Stats
{
    atomic_ullong puts, dels, gets, takes, keys;
};

typedef struct _Stats Stats;

void stats_create(Stats * stats);

void add_put(Stats*);
void add_get(Stats*);
void add_del(Stats*);
void add_take(Stats*);

#endif
