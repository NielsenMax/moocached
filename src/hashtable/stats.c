#include "stats.h"
#include <stdio.h>

void stats_create(Stats* stats)
{
    memset(stats, 0, sizeof(*stats));
}

void add_put(Stats* s)
{
    atomic_fetch_add(&s->puts, 1);
}
void add_get(Stats* s)
{
    atomic_fetch_add(&s->gets, 1);
}
void add_del(Stats* s)
{
    atomic_fetch_add(&s->dels, 1);
}
void add_take(Stats* s)
{
    atomic_fetch_add(&s->takes, 1);
}