#ifndef __LIST_H__
#define __LIST_H__

#include <stddef.h>

#define container_of(ptr, type, member) ((type *)((void *)ptr - offsetof(type, member)))                           

typedef struct _List {
  struct _List* next, * prev;
} List;


// Inicializa una lista
void list_init(List* list);

// Inserta un nodo al comienzo de la lista
void list_push(List* list, List* node);

// Remueve un nodo de la lista
void list_remove(List* node);

#endif
