#include "wrapper.h"

Wrapper* wrapper_create() {
    Wrapper* wr = malloc(sizeof(*wr));
    if (wr == NULL) return  NULL;
    wr->ht = hashtable_create();
    if (wr->ht == NULL) {
        free(wr);
        return NULL;
    }
    stats_create(&wr->stats);
    return wr;
}

int wrapper_put(Wrapper* wr, char* key, char* value, size_t lenkey, size_t lenvalue) {
    add_put(&wr->stats);
    return put(wr->ht, key, value, lenkey, lenvalue);
}

int wrapper_del(Wrapper* wr, char* key, size_t lenkey) {
    add_del(&wr->stats);
    return del(wr->ht, key, lenkey);
}
int wrapper_get(Wrapper* wr, char* key, size_t lenkey, char** value, size_t* len) {
    add_get(&wr->stats);
    return get(wr->ht, key, lenkey, value, len);
}
int wrapper_take(Wrapper* wr, char* key, size_t lenkey, char** value, size_t* len) {
    add_take(&wr->stats);
    return take(wr->ht, key, lenkey, value, len);
}

void* wrapper_tryalloc(Wrapper* wr, size_t size) {
    return tryalloc(wr->ht, size);
}

int wrapper_stats(Wrapper* wr, char** out, size_t* len) {
    // Puede resultar en inconsistencias entre stats pero es aceptable ya que es una 
    // medida meramente estadistica
    Stats stats = wr->stats;
    stats.keys = wr->ht->nodes;
    char fmt[] = "PUTS=%lld DELS=%lld GETS=%lld TAKES=%lld KEYS=%lld";
    if (*out == NULL) {
        *len = snprintf(*out, 0, fmt, stats.puts, stats.dels, stats.gets, stats.takes, stats.keys);
        *out = wrapper_tryalloc(wr, *len);
        if (*out == NULL) return OOMERROR;
    }
    *len = sprintf(*out, fmt, stats.puts, stats.dels, stats.gets, stats.takes, stats.keys);
    return OK;
}