#ifndef __HASH_H__
#define __HASH_H__

#include <stdlib.h>

size_t fhash(char *data , size_t length);

#endif
