#ifndef __TABLE_H__
#define __TABLE_H__

#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

#include "stats.h"
#include "hash.h"
#include "list.h"
#include "../utils.h"
#include "../response.h"

#define SIZE (1 << 19)
#define MAX_EVICT_TRIES 60
#define MAX_MALLOC_TRIES 10
#define R 13

typedef struct _Row Row;
typedef struct _Node Node;
typedef struct _HT HT;

struct _Row
{
  List nodes;
  pthread_mutex_t lock;
};

struct _Node
{
  unsigned long long hash;
  char* key;
  char* value;

  size_t lenkey;
  size_t lenvalue;

  Row* row;

  List lru;
  List collisions;
};

struct _HT
{
  Row rows[SIZE];

  pthread_mutex_t LRUlock;
  List LRU;

  atomic_ullong nodes;
};

void* tryalloc(HT* ht, size_t size);

HT* hashtable_create();
//int create_hash_table(HT* ht);
int evict(HT *ht);
int put(HT *ht,  char* key,  char* value, size_t lenkey, size_t lenvalue);
int del(HT *ht,  char* key, size_t lenkey);
int get(HT *ht,  char* key, size_t lenkey,  char** value, size_t* len);
int take(HT *ht,  char* key, size_t lenkey,  char** value, size_t* len);

#endif
