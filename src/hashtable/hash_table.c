#define _GNU_SOURCE
#include "list.h"
#include "hash_table.h"

static Node* _node_from_lru(List* list) { return container_of(list, Node, lru); }

static Node* _node_from_collisions(List* list) {
  return container_of(list, Node, collisions);
}

HT* hashtable_create() {
  HT* ht = malloc(sizeof(*ht));

  if (ht == NULL) {
    return NULL;
  }

  list_init(&ht->LRU);
  if (pthread_mutex_init(&ht->LRUlock, NULL) != 0)
  {
    free(ht);
    return NULL;
  }
  int i;
  pthread_mutexattr_t attr;

  if (pthread_mutexattr_init(&attr) != 0) {
    goto preerror;
  }
  if (pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE) != 0) {
    goto atrrerror;
  }
  for (i = 0; i < SIZE; ++i) {
    Row* row = &ht->rows[i];
    if (pthread_mutex_init(&row->lock, &attr) != 0) {
      goto error;
    }
    list_init(&row->nodes);
  }
  return ht;
error:
  for (int j = i - 1; j >= 0; j--)
  {
    Row* row = &ht->rows[j];
    pthread_mutex_destroy(&row->lock);
  }
atrrerror:
  pthread_mutexattr_destroy(&attr);
preerror:
  pthread_mutex_destroy(&ht->LRUlock);
  return NULL;
}

static void _increment_node_count(HT* ht) {
  atomic_fetch_add(&ht->nodes, 1);
}

static void _decrement_node_count(HT* ht) {
  atomic_fetch_sub(&ht->nodes, 1);
}

static void _free_node(Node* node) {
  if (node == NULL) return;
  free(node->key);
  free(node->value);
  free(node);
}

int evict(HT* ht)
{
  debug_log("\n\n\nEVICT SPAWNED\n\n\n");

  pthread_mutex_lock(&ht->LRUlock);

  Node* to_free = NULL;
  List* iter = &ht->LRU;
  int i;
  for (i = 0; i < MAX_EVICT_TRIES; i++) {
    iter = iter->prev;
    if (iter == &ht->LRU) {
      break;
    }

    Node* node = _node_from_lru(iter);
    if (pthread_mutex_trylock(&node->row->lock) == 0) {
      to_free = node;
      list_remove(&node->lru);
      list_remove(&node->collisions);
      _decrement_node_count(ht);
      pthread_mutex_unlock(&node->row->lock);
      break;
    }
  }
  pthread_mutex_unlock(&ht->LRUlock);
  if (i == MAX_EVICT_TRIES) return MAXRETRIES;

  _free_node(to_free);

  return OK;
}

void* tryalloc(HT* ht, size_t size)
{

  for (int i = 0; i < MAX_MALLOC_TRIES; i++)
  {
    void* aux = malloc(size);
    if (aux != NULL)
      return aux;

    if (evict(ht) != OK)
    {
      debug_log("\nEVICT NOS DIO NULL\n");
      return NULL;
    }
  }
  debug_log("\nEVICT NOS DIO NULL\n");

  return NULL;
}

static bool _equal_keys(size_t hasha, char* keya, size_t lenkeya, size_t hashb, char* keyb, size_t lenkeyb)
{
  return hasha == hashb && lenkeya == lenkeyb && memcmp(keya, keyb, lenkeya) == 0;
}

// Hay que tener el lock sobre la row
static Node* _findnode(List* list, size_t hash, char* key, size_t lenkey)
{
  List* i;
  for (i = list->next; i != list; i = i->next) {
    Node* node = _node_from_collisions(i);
    if (_equal_keys(node->hash, node->key, node->lenkey, hash, key, lenkey)) {
      break;
    }
  }
  Node* node = _node_from_collisions(i);
  if (node == _node_from_collisions(list)) {
    return NULL;
  }
  else {
    return node;
  }
}

static void _update_lru(HT* ht, Node* node) {
  pthread_mutex_lock(&ht->LRUlock);
  list_remove(&node->lru);
  list_push(&ht->LRU, &node->lru);
  pthread_mutex_unlock(&ht->LRUlock);
}

static void _update_node(Node* node, size_t hash, char* key, char* value, size_t lenkey, size_t lenvalue) {
  if (node == NULL) return;
  node->lenkey = lenkey;
  node->key = key;
  node->lenvalue = lenvalue;
  node->value = value;
  node->hash = hash;
}

static Row* _find_row(HT* ht, size_t hash) {
  int index = hash % SIZE;
  return &ht->rows[index];
}

int put(HT* ht, char* key, char* value, size_t lenkey, size_t lenvalue) {
  size_t hash = fhash(key, lenkey);
  Row* row = _find_row(ht, hash);
  pthread_mutex_lock(&row->lock);
  Node* node = _findnode(&row->nodes, hash, key, lenkey);
  if (node == NULL) {
    node = tryalloc(ht, sizeof(*node));
    if (node == NULL) return OOMERROR;
    node->row = row;
    _update_node(node, hash, key, value, lenkey, lenvalue);
    list_init(&node->collisions);
    list_push(&row->nodes, &node->collisions);
    list_init(&node->lru);
    _update_lru(ht, node);
    pthread_mutex_unlock(&row->lock);
    _increment_node_count(ht);
    return OK;
  }
  else {
    char* oldkey = node->key;
    char* oldvalue = node->value;
    _update_node(node, hash, key, value, lenkey, lenvalue);
    _update_lru(ht, node);
    pthread_mutex_unlock(&row->lock);
    free(oldkey);
    free(oldvalue);
    return OK;
  }
}

int del(HT* ht, char* key, size_t lenkey) {
  size_t hash = fhash(key, lenkey);
  Row* row = _find_row(ht, hash);
  pthread_mutex_lock(&row->lock);
  Node* node = _findnode(&row->nodes, hash, key, lenkey);
  if (node != NULL) {
    list_remove(&node->collisions);
    pthread_mutex_lock(&ht->LRUlock);
    list_remove(&node->lru);
    pthread_mutex_unlock(&ht->LRUlock);
    pthread_mutex_unlock(&row->lock);
    _decrement_node_count(ht);
    return OK;
  }
  pthread_mutex_unlock(&row->lock);
  return ENOTFOUND;
}

int get(HT* ht, char* key, size_t lenkey, char** value, size_t* len) {
  size_t hash = fhash(key, lenkey);
  Row* row = _find_row(ht, hash);
  pthread_mutex_lock(&row->lock);
  Node* node = _findnode(&row->nodes, hash, key, lenkey);
  if (node != NULL) {
    *len = node->lenvalue;
    *value = tryalloc(ht, node->lenvalue);

    if (*value == NULL) {
      pthread_mutex_unlock(&row->lock);
      return OOMERROR;
    }

    memcpy(*value, node->value, *len);
    _update_lru(ht, node);

    pthread_mutex_unlock(&row->lock);
    return OK;
  }
  pthread_mutex_unlock(&row->lock);
  return ENOTFOUND;
}

int take(HT* ht, char* key, size_t lenkey, char** value, size_t* len) {
  size_t hash = fhash(key, lenkey);
  Row* row = _find_row(ht, hash);
  pthread_mutex_lock(&row->lock);
  Node* node = _findnode(&row->nodes, hash, key, lenkey);
  if (node != NULL) {
    *len = node->lenvalue;
    *value = node->value;
    list_remove(&node->collisions);

    pthread_mutex_lock(&ht->LRUlock);
    list_remove(&node->lru);
    pthread_mutex_unlock(&ht->LRUlock);

    pthread_mutex_unlock(&row->lock);
    _decrement_node_count(ht);

    free(node->key);
    free(node);

    return OK;
  }
  pthread_mutex_unlock(&row->lock);
  return ENOTFOUND;
}

