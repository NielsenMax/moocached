#include "list.h"

void list_init(List *list)
{
    list->next = list;
    list->prev = list;
}

void list_push(List *list, List *node)
{
    List *next = list->next;

    node->prev = list;
    node->next = next;

    next->prev = node;
    list->next = node;
}

void list_remove(List *node)
{
    List *prev = node->prev;
    List *next = node->next;

    prev->next = next;
    next->prev = prev;

    node->prev = node;
    node->next = node;
}