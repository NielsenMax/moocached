#ifndef __WRAPPER_H__
#define __WRAPPER_H__

#include "hash_table.h"
#include "stats.h"

typedef struct _Wrapper Wrapper;

struct _Wrapper {
    Stats stats;
    HT* ht;
};

Wrapper* wrapper_create();

int wrapper_put(Wrapper* wr, char* key, char* value, size_t lenkey, size_t lenvalue);
int wrapper_del(Wrapper* wr, char* key, size_t lenkey);
int wrapper_get(Wrapper* wr, char* key, size_t lenkey, char** value, size_t* len);
int wrapper_take(Wrapper* wr, char* key, size_t lenkey, char** value, size_t* len);
void* wrapper_tryalloc(Wrapper* wr, size_t size);
int wrapper_stats(Wrapper* wr, char** out, size_t *len);

#endif
