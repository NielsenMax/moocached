#include "hash.h"

#define R 13ul

size_t fhash(char *data , size_t length){
    size_t hsh = 0;
    size_t i;
    size_t r = 1;
    for(i = 0; i < length; i++){
        hsh = (hsh + r * (*data +  128));
        data++;
        r = (r*R);
    }
    return hsh;
}

