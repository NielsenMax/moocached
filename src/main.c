#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/resource.h>
#include "utils.h"

#define DEFAULT_TEXT_SOCKET 888
#define DEFAULT_BIN_SOCKET 889
#define DEFAULT_MEM_LIMIT 4 * (1 << 20) /// 4 MB, abajo se pasan a gigas.

int mk_lsock(int port)
{
	struct sockaddr_in sa;
	int lsock;
	int rc;
	int yes = 1;

	/* Crear socket */
	lsock = socket(AF_INET, SOCK_STREAM, 0);
	if (lsock < 0)
		quit("socket");

	/* Setear opción reuseaddr... normalmente no es necesario */
	if (setsockopt(lsock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == 1)
		quit("setsockopt");

	sa.sin_family = AF_INET;
	sa.sin_port = htons(port); /// este puerto solo se puede bindear siendo root.
	sa.sin_addr.s_addr = htonl(INADDR_ANY);

	/* Bindear al puerto port TCP, en todas las direcciones disponibles */
	rc = bind(lsock, (struct sockaddr *)&sa, sizeof sa);
	if (rc < 0)
		quit("bind");

	/* Setear en modo escucha */
	rc = listen(lsock, 10);
	if (rc < 0)
		quit("listen");

	return lsock;
}

char *itos(int i)
{
	char *str = malloc(sizeof(char) * 6);
	sprintf(str, "%d", i);
	return str;
}

bool std_down_privileges()
{

	if (getuid() != 0)
		return true;
	if ((setgid(1000) == -1) || (setuid(1000) == -1))
	{
		return false;
	}
	if (getuid() != 0)
		return true;
	return false;
}

int tsocket = DEFAULT_TEXT_SOCKET;
int bsocket = DEFAULT_BIN_SOCKET;
unsigned long mlimit = DEFAULT_MEM_LIMIT;

void parseargs(int argc, char **argv)
{
	/// argv[0] = nombre del programa.
	for (int i = 1; i < argc - 1; i++)
	{

		if (strcmp(argv[i], "-t") == 0)
			tsocket = atoi(argv[i + 1]);

		if (strcmp(argv[i], "-b") == 0)
			bsocket = atoi(argv[i + 1]);

		if (strcmp(argv[i], "-m") == 0)
			mlimit = atoi(argv[i + 1]);
	}
}

int main(int argc, char **argv)
{

	parseargs(argc, argv);

	uid_t uid = getuid(); /// les pasamos user y grupo de usuario no root.

	if (uid != 0)
	{
		quit("Se necesitan permisos de root.");
	}

	int text_socket = mk_lsock(tsocket);
	int bin_socket = mk_lsock(bsocket);

	/*el rlim_cur ponerle 10MB de mas por las dudas*/
	struct rlimit *rlimits = malloc(sizeof(struct rlimit));
	rlimits->rlim_cur = mlimit * 1000000lu;		/// limite que se puede editar por un proceso no root.
	rlimits->rlim_max = rlimits->rlim_cur; /// limite que puede poner un proceso root

	if (setrlimit(RLIMIT_DATA, rlimits) == -1)
	{
		quit("No se pudo limitar la memoria");
	}
	free(rlimits);

	if (!std_down_privileges())
	{
		bool down_privileges = false;
		while (!down_privileges)
		{
			printf("Ingrese GROUPID y USERID de usuario no root\n");
			int gid, uid;
			scanf("%d %d", &gid, &uid);
			if (setgid(gid) == -1)
			{
				printf("No se puede cambiar al GROUPID: %d\n", gid);
				continue;
			}
			if (setuid(uid) == -1)
			{
				printf("No se puede cambiar al USERID: %d\n", uid);
				continue;
			}
			if (getuid() != 0)
				down_privileges = true;
		}
	}

	if (access("./build/server", F_OK) == 0)
	{
		// execl("/usr/bin/valgrind","/usr/bin/valgrind","./build/server", itos(text_socket), itos(bin_socket), NULL);

		execl("./build/server","./build/server", itos(text_socket), itos(bin_socket), NULL);
	}
	else
	{
		quit("El server no fue buildeado");
	}

	return 0;
}
