# Trabajo Practico Final Sistemas Operativos I

## Integrantes

- Luis Aseguinolaza (A-4377/1)
- Maximiliano Nielsen (N-1218/1)

## Estructura del projecto

- `src/server` implementa todo lo relacionado con el servidor tcp.
- `src/hashtable` implementa una tabla hash concurrente con un sistema de LRU.
- `src/client` implementa un modulo de Erlang que utiliza al servidor en modo binario
- `src/main.c` inicia el servidor y realiza la bajada de privilegios.

## Como ejecutar el servidor

El comando `make` deberia buildearlo y ejecutarlo.

Si se quiere tener logs de debug, podemos utilizar el comando `make debug`.

Si se desea hacer por partes:

- `make build/server`
- `make build/main`
- `sudo ./build/main`

El servidor cuenta con los argumentos -t -b -m donde se pueden asignar el socket de texto, el socket binario y el limite de memoria (en MB) respectivamente.

## Estructuras de datos

### Almacenamiento clave valor

Para poder almacenar los pares clave valor (KV) decidimos implementar una tabla hash que en las colisiones utiliza una lista intrusiva doblemente enlazada circular.

La misma utiliza un lock por cada una de estas listas, este sera tanto de escritura de como de lectura. Consideramos que no es terrible tener un solo lock por lista ya que los casos de colisiones deberian ser pocos.

### LRU

Esta consiste en una lista intrusiva de nodos doblemente enlazada circular. Cuando un nodo se crea, updatea o es leido en una operacion **GET** se mueve este nodo al inicio de la lista.

Cuando se llama a la operacion **evict**, se comienza a recorrer la lista de forma ciclica, y en orden inverso, hasta encontrar un nodo cuyo lock de fila en la tabla hash este disponible, luego se toma dicho lock y se procede con la eliminacion del nodo.

La LRU cuenta con un unico lock que debe ser tomado siempre que se quiera insertar o eliminar, o mover un nodo.

### Estadisticas

Para la implementacion de las estadisticas optamos por utilizar valores positivos modificados por operaciones atomicas para evitar un cuello de botella entre escrituras y lecturas.
Como el proceso de obtener las estadisticas no es atomico, se pueden tener inconsistencias, las cuales aceptamos por ser una medida meramente estadistica.

## Manejo de conexiones

Para manejar las conexiones utilizamos EPOLL para recibir conexiones en los puertos de texto y binario.
Levantamos tantos threads como hilos tenga el procesador y los ponemos a recibir consultas.
Ante cualquier error, optamos por el mejor esfuerzo, es decir, cerramos la conexion y liberamos su informacion asociada.
Contamos con estructuras que permiten guardar consultas parcialmente por lo que es posible que se registren multiples veces los sockets en el EPOLL.

### Referencia

https://www.data-structures-in-practice.com/intrusive-linked-lists/
https://github.com/torvalds/linux/blob/master/include/linux/list.h
